import React from "react"
import Provider from "./components/Provider"

const App = () => <Provider>Hello Manjoc!</Provider>

export default App
