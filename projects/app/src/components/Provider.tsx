import {ThemeProvider} from "styled-components"

import "../index.css"
import theme from "../theme"

interface ProviderProps {
  children: React.ReactNode
}

const Provider = (props: ProviderProps) => (
  <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
)

export default Provider
