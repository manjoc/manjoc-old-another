import {
  Button as RebassButton,
  ButtonProps as RebassButtonProps,
} from "rebass/styled-components"

interface ButtonProps extends RebassButtonProps {
  title: string
  variant: "plain" | "outline" | "ghost"
}

const Button = ({title, variant = "plain", ...rest}: ButtonProps) => (
  <RebassButton
    sx={{
      borderRadius: "circle",
      borderStyle: "solid",
      borderWidth: "default",
      fontSize: "body",
      fontWeight: "bold",
    }}
    variant={variant}
    {...rest}
  >
    {title}
  </RebassButton>
)

export default Button
