const theme = {
  borderWidths: {
    default: "2px",
  },

  buttons: {
    plain: {
      bg: "primary",
      borderColor: "primary",
      color: "white",
    },
    outline: {
      bg: "transparent",
      borderColor: "primary",
      color: "primary",
    },
    ghost: {
      bg: "transparent",
      borderColor: "transparent",
      color: "primary",
    },
  },

  colors: {
    primary: "hsl(0, 0%, 25%)",
    white: "hsl(0, 0%, 100%)",

    greys: {
      lightest: "hsl(0, 0%, 85%)",
    },
  },

  fonts: {
    body: "Open Sans",
  },
  fontSizes: {
    body: 16,
  },
  fontWeights: {
    body: 400,
    bold: 700,
  },

  radii: {
    circle: "2em",
    default: "8px",
  },

  sizes: [4, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 640, 768],
  space: [4, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 640, 768],
}

export default theme
