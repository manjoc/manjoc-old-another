const path = require("path")
module.exports = {
  getExampleFilename(componentPath) {
    return componentPath.replace("components", "docs").replace(".tsx", ".md")
  },
  ignore: ["**/src/components/Provider.tsx"],
  styleguideComponents: {
    Wrapper: path.join(__dirname, "src/components/Provider.tsx"),
  },
}
